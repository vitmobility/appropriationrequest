/*global QUnit*/

jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/varian/approreq/AppropriationRequest/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/varian/approreq/AppropriationRequest/test/integration/pages/App",
	"com/varian/approreq/AppropriationRequest/test/integration/pages/Browser",
	"com/varian/approreq/AppropriationRequest/test/integration/pages/Master",
	"com/varian/approreq/AppropriationRequest/test/integration/pages/Detail",
	"com/varian/approreq/AppropriationRequest/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.varian.approreq.AppropriationRequest.view."
	});

	sap.ui.require([
		"com/varian/approreq/AppropriationRequest/test/integration/NavigationJourneyPhone",
		"com/varian/approreq/AppropriationRequest/test/integration/NotFoundJourneyPhone",
		"com/varian/approreq/AppropriationRequest/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});